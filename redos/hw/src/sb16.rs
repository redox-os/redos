use alloc::{string::String, vec::Vec};
use hwio::{Io, Pio, ReadOnly, WriteOnly};

use super::isadma::{IsaDma, IsaDmaBuffer, IsaDmaChannel};

#[allow(dead_code)]
pub struct Sb16 {
    pub version: (u8, u8),
    pub irqs: Vec<u8>,
    pub dmas: Vec<IsaDmaChannel>,
    pub buffer: IsaDmaBuffer,
    // Regs
    /* 0x04 */ mixer_addr: WriteOnly<Pio<u8>>,
    /* 0x05 */ mixer_data: Pio<u8>,
    /* 0x06 */ dsp_reset: WriteOnly<Pio<u8>>,
    /* 0x0A */ dsp_read_data: ReadOnly<Pio<u8>>,
    /* 0x0C */ dsp_write_data: WriteOnly<Pio<u8>>,
    /* 0x0C */ dsp_write_status: ReadOnly<Pio<u8>>,
    /* 0x0E */ dsp_read_status: ReadOnly<Pio<u8>>,
    /* 0x0F */ irq_acknowledge: ReadOnly<Pio<u8>>,
}

impl Sb16 {
    pub unsafe fn new<D: Fn(u32)>(addr: u16, delay_us: D) -> Result<Self, String> {
        let mut module = Sb16 {
            version: (0, 0),
            irqs: Vec::new(),
            dmas: Vec::new(),
            buffer: IsaDmaBuffer::new()
                .ok_or_else(|| format!("failed to allocate ISA DMA buffer"))?,
            // Regs
            mixer_addr: WriteOnly::new(Pio::new(addr + 0x04)),
            mixer_data: Pio::new(addr + 0x05),
            dsp_reset: WriteOnly::new(Pio::new(addr + 0x06)),
            dsp_read_data: ReadOnly::new(Pio::new(addr + 0x0A)),
            dsp_write_data: WriteOnly::new(Pio::new(addr + 0x0C)),
            dsp_write_status: ReadOnly::new(Pio::new(addr + 0x0C)),
            dsp_read_status: ReadOnly::new(Pio::new(addr + 0x0E)),
            irq_acknowledge: ReadOnly::new(Pio::new(addr + 0x0F)),
        };

        module.init(delay_us)?;

        Ok(module)
    }

    fn mixer_read(&mut self, index: u8) -> u8 {
        self.mixer_addr.write(index);
        self.mixer_data.read()
    }

    fn mixer_write(&mut self, index: u8, value: u8) {
        self.mixer_addr.write(index);
        self.mixer_data.write(value);
    }

    fn dsp_read(&mut self) -> Result<u8, String> {
        log::info!("DSP read");

        // Bit 7 must be 1 before data can be sent
        while !self.dsp_read_status.readf(1 << 7) {
            //TODO: timeout!
        }

        let value = self.dsp_read_data.read();
        log::info!("DSP read = Ok(0x{:02X})", value);
        Ok(value)
    }

    fn dsp_write(&mut self, value: u8) -> Result<(), String> {
        log::info!("DSP write 0x{:02X}", value);

        // Bit 7 must be 0 before data can be sent
        while self.dsp_write_status.readf(1 << 7) {
            //TODO: timeout!
        }

        self.dsp_write_data.write(value);
        log::info!("DSP write = Ok(())");
        Ok(())
    }

    fn init<D: Fn(u32)>(&mut self, delay_us: D) -> Result<(), String> {
        // Perform DSP reset
        {
            // Write 1 to reset port
            self.dsp_reset.write(1);

            // Wait 3us
            delay_us(3);

            // Write 0 to reset port
            self.dsp_reset.write(0);

            //TODO: Wait for ready byte (0xAA) using read status
            delay_us(100);

            let ready = self.dsp_read()?;
            if ready != 0xAA {
                return Err(format!("Ready byte was 0x{:02X} instead of 0xAA", ready));
            }
        }

        // Read DSP version
        {
            self.dsp_write(0xE1)?;

            let major = self.dsp_read()?;
            let minor = self.dsp_read()?;
            self.version = (major, minor);

            if major != 4 {
                return Err(format!("Unsupported DSP major version {}", major));
            }
        }

        // Get available IRQs and DMAs
        {
            self.irqs.clear();
            let irq_mask = self.mixer_read(0x80);
            if (irq_mask & (1 << 0)) != 0 {
                self.irqs.push(2);
            }
            if (irq_mask & (1 << 1)) != 0 {
                self.irqs.push(5);
            }
            if (irq_mask & (1 << 2)) != 0 {
                self.irqs.push(7);
            }
            if (irq_mask & (1 << 3)) != 0 {
                self.irqs.push(10);
            }

            self.dmas.clear();
            let dma_mask = self.mixer_read(0x81);
            if (dma_mask & (1 << 0)) != 0 {
                self.dmas.push(IsaDmaChannel::Zero);
            }
            if (dma_mask & (1 << 1)) != 0 {
                self.dmas.push(IsaDmaChannel::One);
            }
            if (dma_mask & (1 << 3)) != 0 {
                self.dmas.push(IsaDmaChannel::Three);
            }
            if (dma_mask & (1 << 5)) != 0 {
                self.dmas.push(IsaDmaChannel::Five);
            }
            if (dma_mask & (1 << 6)) != 0 {
                self.dmas.push(IsaDmaChannel::Six);
            }
            if (dma_mask & (1 << 7)) != 0 {
                self.dmas.push(IsaDmaChannel::Seven);
            }
        }

        // Set master volume
        self.mixer_write(0x22, 0xFF);

        // Turn speaker on
        self.dsp_write(0xD1)?;

        Ok(())
    }

    pub fn start(&mut self) -> Result<(), String> {
        // Start DMA transfers
        {
            let mut dma_opt = None;
            for &dma_channel in &self.dmas {
                match dma_channel {
                    IsaDmaChannel::Five | IsaDmaChannel::Six | IsaDmaChannel::Seven => {
                        dma_opt = Some(IsaDma::new(dma_channel));
                        break;
                    }
                    _ => (),
                }
            }

            let mut dma = match dma_opt {
                Some(some) => some,
                None => return Err(format!("No compatible DMA channel found")),
            };

            log::info!("DMA {}, {}", dma.primary, dma.index);

            // Disable channel
            dma.mask.write(0x04 | dma.index);
            // Flip flop reset (any value)
            dma.flip_flop.write(0xFF);
            // Set transfer mode to automatic
            dma.mode.write(0x58 | dma.index);
            // Write page
            dma.page.write((self.buffer.address() >> 16) as u8);
            // Write address >> 1, low byte
            dma.address.write((self.buffer.address() >> 1) as u8);
            // Write address >> 1, high byte
            dma.address.write((self.buffer.address() >> 9) as u8);
            // Write length (0x1_0000 - 1) >> 1, low byte
            dma.count.write(0xFF);
            // Write length (0x1_0000 - 1) >> 1, high byte
            dma.count.write(0x7F);
            // Enable channel
            dma.mask.write(dma.index);
        }

        // Set output sample rate to 44100 Hz (Redox OS standard)
        {
            let rate = 44100u16;
            self.dsp_write(0x41)?;
            self.dsp_write((rate >> 8) as u8)?;
            self.dsp_write(rate as u8)?;
        }

        // Set sound format to 16-bit stereo signed
        {
            // 16-bit DMA with auto-init and FIFO
            self.dsp_write(0xB6)?;
            // Stereo, signed samples
            self.dsp_write(0x30)?;
            // Number of samples minus one, low
            self.dsp_write(0xFF)?;
            // Number of samples minus one, high
            self.dsp_write(0x3F)?;
        }

        Ok(())
    }

    pub fn wait(&mut self) -> Result<(), String> {
        //TODO: timeout
        while self.mixer_read(0x82) == 0 {}
        self.irq_acknowledge.read();
        Ok(())
    }

    pub fn stop(&mut self) -> Result<(), String> {
        self.dsp_write(0xD9)?;
        Ok(())
    }
}
