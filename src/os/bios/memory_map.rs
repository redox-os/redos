use core::{cmp, mem, ptr};

use crate::os::{OsMemoryEntry, OsMemoryKind};

use super::{thunk::ThunkData, MEMORY_MAP_ADDR};

#[repr(packed)]
struct MemoryMapEntry {
    pub base: u64,
    pub size: u64,
    pub kind: u32,
}

pub struct MemoryMapIter {
    thunk15: extern "C" fn(),
    data: ThunkData,
    first: bool,
}

impl MemoryMapIter {
    pub fn new(thunk15: extern "C" fn()) -> Self {
        Self {
            thunk15,
            data: ThunkData::new(),
            first: true,
        }
    }
}

impl Iterator for MemoryMapIter {
    type Item = OsMemoryEntry;
    fn next(&mut self) -> Option<Self::Item> {
        if self.first {
            self.first = false;
        } else if self.data.ebx == 0 {
            return None;
        }

        self.data.eax = 0xE820;
        self.data.ecx = mem::size_of::<MemoryMapEntry>() as u32;
        self.data.edx = 0x534D4150;
        self.data.edi = MEMORY_MAP_ADDR as u32;

        unsafe {
            self.data.with(self.thunk15);
        }

        //TODO: return error?
        if self.data.eax != 0x534D4150 {
            println!("INT 15h, AX=E820h not supported");

            // Fall back to INT 15h, AH=88h
            self.data = ThunkData::new();
            self.data.eax = 0x8800;

            unsafe {
                self.data.with(self.thunk15);
            }

            // Return None on next
            self.data.ebx = 0;

            if self.data.eax > 0 {
                return Some(Self::Item {
                    base: 0x100000,
                    size: (self.data.eax as u64) * 1024,
                    kind: OsMemoryKind::Free,
                });
            }

            println!("INT 15h, AH=88h not supported");
            return None;
        }

        assert_eq!({ self.data.ecx }, mem::size_of::<MemoryMapEntry>() as u32);

        let entry = unsafe { ptr::read(MEMORY_MAP_ADDR as *const MemoryMapEntry) };
        Some(Self::Item {
            base: entry.base,
            size: entry.size,
            kind: match entry.kind {
                0 => OsMemoryKind::Null,
                1 => OsMemoryKind::Free,
                3 => OsMemoryKind::Reclaim,
                _ => OsMemoryKind::Reserved,
            },
        })
    }
}

pub unsafe fn memory_map(thunk15: extern "C" fn()) -> Option<(usize, usize)> {
    let mut heap_limits = None;
    for entry in MemoryMapIter::new(thunk15) {
        let heap_start = 2 * 1024 * 1024;
        if { entry.kind } == OsMemoryKind::Free
            && entry.base <= heap_start as u64
            && (entry.base + entry.size) >= heap_start as u64
        {
            let heap_end = cmp::min(entry.base + entry.size, usize::MAX as u64) as usize;
            if heap_end >= heap_start {
                heap_limits = Some((heap_start, heap_end - heap_start));
            }
        }
    }
    heap_limits
}
